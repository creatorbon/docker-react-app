import React from "react";
import { connect } from "react-redux";
import moment from "moment";
import styles from "./styles.module.scss";

const ChatHeader = ({ messages }) => {
  let messageCount = 0;
  let userCount = 0;
  let lastMessage = 0;

  if (messages) {
    messageCount = messages?.length || 0;
    userCount = new Set(messages?.map((message) => message.user)).size;
    lastMessage = messages[messageCount - 1]?.createdAt;
  }

  const lastMessageTime = moment(lastMessage).format("LT");

  return (
    <div className={styles.chatHeader}>
      <div className="">My Chat</div>
      <div className="">{`${userCount} participants`}</div>
      <div className="">{`${messageCount} messages`}</div>
      <div className="">{`Last message at ${lastMessageTime}`}</div>
    </div>
  );
};

const mapStateToProps = ({ chat }) => ({
  messages: chat.messages,
});

export default connect(mapStateToProps)(ChatHeader);
