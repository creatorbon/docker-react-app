import React, { useEffect, useRef } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { setMessage, editMessage } from "../../redux/chatAction";
import { setUser } from "../../redux/userAction";
import MessageInput from "../MessageInput";
import MessageList from "../MessageList";
import ChatHeader from "../ChatHeader";
import fakeData from "../../utils/api";

const API_URL = "https://edikdolynskyi.github.io/react_sources/messages.json";
const USER = {
  user: "Ben",
  avatar: "https://www.aceshowbiz.com/images/photo/tom_pelphrey.jpg",
  userId: "5328dba1-1b8f-11e8-9629-c7eca82aa7bd",
};

const Chat = ({
  setMessage,
  editMessage,
  setUser,
  messages,
  user,
}) => {

  const inputRef = useRef(null);

  useEffect(() => {
    fetch(API_URL)
      .then((res) => res.json())
      .then((result) => {
        if (result) setMessage(result);
        else setMessage(fakeData);
      });
    setUser(USER);
  }, [ setMessage, setUser]);

  useEffect(() => {
    const editLastMessage = (event) => {
      const allUserMessages = messages.filter(
        (message) => message.userId === user.userId
      );

      if (allUserMessages.length && event.key === "ArrowUp") {
        const lastMessage = allUserMessages.slice(-1)[0];
        editMessage(lastMessage);
      }
    };

    document.addEventListener("keydown", editLastMessage);

    return () => document.removeEventListener("keydown", editLastMessage);
  }, [editMessage, messages, user.userId]);


  useEffect(() => {
    inputRef.current.focus();
  }, [messages]);


  return (
    <>
      <ChatHeader></ChatHeader>
      <MessageList></MessageList>
      <MessageInput inputRef={inputRef}></MessageInput>
    </>
  );
};

const mapStateToProps = ({ chat, user }) => ({
  messages: chat.messages,
  user: user,
  isLoaded: chat.isLoaded,
});

const actions = { setMessage, setUser, editMessage };

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
