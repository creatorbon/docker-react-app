import React, { useRef, useEffect } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  removeMessage,
  likeMessage,
  editMessage,
} from "../../redux/chatAction";
import Message from "../Message";
import styles from "./styles.module.scss";
import Spinner from "../Spinner";
import TimeLine from "../TimeLine";
import * as groupBy from "lodash/groupBy";

const MessageList = ({
  user,
  messages,
  isLoaded,
  removeMessage,
  editMessage,
  likeMessage,
}) => {
  const groupedData = groupBy(messages, (message) => {
    return message.createdAt.split("T")[0];
  });
  const messagesRef = useRef(null);

  useEffect(() => {
    if (messagesRef.current) {
      messagesRef.current.scrollTo(0, 99999);
    }
  }, [messages]);

  const renderGroupedData = (groups) =>
    Object.keys(groups).map((group) => {
      return (
        <React.Fragment key={group}>
          <TimeLine data={group} />
          {groups[group].map((message) => (
            <Message
              key={message.id}
              isMe={user.userId === message.userId}
              time={message.createdAt}
              message={message}
              onDelete={removeMessage}
              onEdit={editMessage}
              onLike={likeMessage}
            />
          ))}
        </React.Fragment>
      );
    });

  if (!isLoaded) {
    return (
      <div className={styles.messageList}>
        <Spinner />
      </div>
    );
  } else {
    return (
      <div ref={messagesRef} className={styles.messageList}>
        {renderGroupedData(groupedData)}
      </div>
    );
  }
};

const mapStateToProps = ({ chat, user }) => ({
  messages: chat.messages,
  isLoaded: chat.isLoaded,
  user: user,
});

const actions = { removeMessage, likeMessage, editMessage };

const mapDispatchToProps = (dispatch) => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(MessageList);
