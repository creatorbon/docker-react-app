import React from "react";
import styles from "./styles.module.scss";

const Avatar = ({ avatar }) => {
  return <img alt="avatar" className={styles.avatar} src={avatar}></img>;
};

export default Avatar;
