import React from "react";
import styles from "./styles.module.scss";
import moment from "moment";

const TimeLine = ({ data }) => {
  const time = moment().format("MMMM Do");

  if (moment(data).format("DD") === moment().format("DD")) {
    return <div className={styles.timeLine}>Today</div>;
  } else if (moment(data).format("DD") - 1 === moment().format("DD")) {
    return <div className={styles.timeLine}>Yesterday</div>;
  } else {
    return <div className={styles.timeLine}>{time}</div>;
  }
};

export default TimeLine;
