import { createStore, combineReducers } from "redux";
import chatReducer from "./redux/chatReducer";
import userReducer from "./redux/userReducer";
import { composeWithDevTools } from "redux-devtools-extension";

const initialState = {};

const reducers = {
  chat: chatReducer,
  user: userReducer,
};

const rootReducer = combineReducers({ ...reducers });

const store = createStore(rootReducer, initialState, composeWithDevTools());

export default store;
